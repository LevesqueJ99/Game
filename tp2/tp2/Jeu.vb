﻿'*****************************************************************************************************
'Fichier:Tp2
'
'Auteur: Joey Levesque

'Classe: Jeu
'
'Utilite: Permet de contenir une liste de joueur serializable, une liste de
'high-score et les noms de ceux qui ont fait les high scores.
'De cette maniere il est possible de sauvegarder tous les high-scores
'et de sauvegarder une partie avec la liste de joueurs
'
'*****************************************************************************************************
<Serializable>
Public Class Jeu
    Private _listPersonnage As List(Of Personnage)
    Private _strNomHighScore As List(Of String)
    Private _iHighScore As List(Of Integer)
    Private _nbDeplacement As Integer

#Region "Get/Set"
    Public Property nbDeplacement() As Integer
        Get
            Return _nbDeplacement
        End Get
        Set(ByVal value As Integer)
            _nbDeplacement = value
        End Set
    End Property
    Public Property HighScore() As List(Of Integer)
        Get
            Return _iHighScore
        End Get
        Set(ByVal value As List(Of Integer))
            _iHighScore = value
        End Set
    End Property
    Public Property NomHighScore() As List(Of String)
        Get
            Return _strNomHighScore
        End Get
        Set(ByVal value As List(Of String))
            _strNomHighScore = value
        End Set
    End Property
    Public Property ListePersonnage() As List(Of Personnage)
        Get
            Return _listPersonnage
        End Get
        Set(ByVal value As List(Of Personnage))
            _listPersonnage = value
        End Set
    End Property
#End Region

    Public Sub New()
        _listPersonnage = New List(Of Personnage)
        _strNomHighScore = New List(Of String)
        _iHighScore = New List(Of Integer)

        _strNomHighScore.Add("Joe")
        _strNomHighScore.Add("AAA")
        _strNomHighScore.Add("John")
        _strNomHighScore.Add("Donald")
        _strNomHighScore.Add("Arya")

        _iHighScore.Add(120)
        _iHighScore.Add(60)
        _iHighScore.Add(45)
        _iHighScore.Add(17)
        _iHighScore.Add(15)
    End Sub

End Class
