﻿'Fichier:Tp2
'
'Classe Citoyen
'
'Auteur: Joey Levesque
'
'Utilite: Cette classe permet de creer un citoyen.
'
'Pourquoi cette classe existe? Cette classe existe et se differencie des autres grace 
'a ces attributs _Mouton de type boolean et _Direction de type Direction. Elle se demarque
'autres classes par le fait que son deplacement est fait par un "algorithme".

<Serializable>
Public Class Citoyen
    Inherits NonJoueur

    Private _Direction As Direction
    Private _Mouton As Boolean
    'L'attributs _nbDeplacement sert a recevoir le nb de deplacement fais dans le jeu et est utile
    'dans la methode Bouger 
    Private _nbdeplacement As Integer = 0
    Private _AEssayeDEtreInfluence As Boolean


#Region "Get/Set"
    Public Property Influencement() As Boolean
        Get
            Return _AEssayeDEtreInfluence
        End Get
        Set(ByVal value As Boolean)
            _AEssayeDEtreInfluence = value
        End Set
    End Property
    Public Property Mouton() As Boolean
        Get
            Return _Mouton
        End Get
        Set(ByVal value As Boolean)
            _Mouton = value
        End Set
    End Property
    Public Property Direction() As Direction
        Get
            Return _Direction
        End Get
        Set(ByVal value As Direction)
            _Direction = value
        End Set
    End Property
    Public Property nbDeplacementJeu() As Integer
        Get
            Return _nbdeplacement
        End Get
        Set(value As Integer)
            _nbdeplacement = value
        End Set
    End Property
#End Region

    Public Sub New(pPhoto As Image, pPosition As Point, dDirection As Direction)
        MyBase.New(pPhoto, pPosition)
        Direction = dDirection
        Influencement = False
        Dim iNbaleatoire As Integer

        iNbaleatoire = GenererNbAleatoire(0, 100)
        If iNbaleatoire > 40 Then
            Mouton = True
        End If

    End Sub
    ''' <summary>
    ''' Reecriture de la methode bouger parceque les citoyens doivent bouger
    ''' en ligne droite selon une direction preetablie.
    ''' Elle permet de modifier la position du citoyen en fonction de son enum deplacement
    ''' </summary>
    Public Overrides Sub Bouger()
        Select Case Direction
            Case Direction.Sud
                Position = New Point(Position.X, Position.Y + 50)
            Case Direction.SudOuest
                If nbDeplacementJeu Mod 4 = 0 Then
                    Position = New Point(Position.X - 50, Position.Y)
                Else
                    Position = New Point(Position.X, Position.Y + 50)
                End If
            Case Direction.Est
                Position = New Point(Position.X + 50, Position.Y)
            Case Direction.SudEst
                If nbDeplacementJeu Mod 4 = 0 Then
                    Position = New Point(Position.X + 50, Position.Y)
                Else
                    Position = New Point(Position.X, Position.Y + 50)
                End If
            Case Direction.Ouest
                Position = New Point(Position.X - 50, Position.Y)
            Case Direction.NordOuest
                If nbDeplacementJeu Mod 4 = 0 Then
                    Position = New Point(Position.X - 50, Position.Y)
                Else
                    Position = New Point(Position.X, Position.Y - 50)
                End If
            Case Direction.Nord
                Position = New Point(Position.X, Position.Y - 50)
            Case Else
                If nbDeplacementJeu Mod 4 = 0 Then
                    Position = New Point(Position.X + 50, Position.Y)
                Else
                    Position = New Point(Position.X, Position.Y - 50)
                End If
        End Select
    End Sub

End Class
