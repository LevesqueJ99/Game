﻿'Fichier: Tp2
'
'Classe: Non-Joueur
'
'Auteur: Joey Levesque
'
'Utilite:Permet de creer un objet Non-Joueur
'
'Pourquoi cette classe existe ? Cette classe existe uniquement pour avoir une methode bouger
'en MustOverride

<Serializable>
Public MustInherit Class NonJoueur
    Inherits Personnage




    Protected Sub New(pPhoto As Image, pPosition As Point)
        MyBase.New(pPhoto, pPosition)

    End Sub

    ''' <summary>
    ''' Cette methode est Overridablee puisque chaque personnage a une maniere speciale de bouger
    ''' sauf le faux refugie et la milice qui, en temps normal ont la meme maniere pour bouger
    ''' 
    ''' </summary>
    Public Overridable Sub Bouger()
        Dim bTermine As Boolean = False
        Do While (bTermine <> True)
            Dim iNbAleatoire As Integer
            iNbAleatoire = GenererNbAleatoire(0, 4)
            Select Case iNbAleatoire
                Case 0
                    If Position.Y > 1 Then
                        Position = New Point(Position.X, Position.Y - 50)
                        bTermine = True
                    End If
                Case 1
                    If Position.Y < 551 Then
                        Position = New Point(Position.X, Position.Y + 50)
                        bTermine = True
                    End If
                Case 2
                    If Position.X > 1 Then
                        Position = New Point(Position.X - 50, Position.Y)
                        bTermine = True
                    End If
                Case 3
                    If Position.X < 551 Then
                        Position = New Point(Position.X + 50, Position.Y)
                        bTermine = True
                    End If
            End Select
        Loop
    End Sub



End Class
