﻿'Fichier Tp2
'
'Auteur: Joey Levesque
'
'Classe: Milice
'
'Utilite: Permet de creer un objet Milice
'
'Pourquoi cette classe existe ? Cette classe se differencie des autres vilains parceque 
'celle-ci doit permettre d'implanter l'interface IInfluencer.La classe Milice se demarque des
'autres vilains parce que sa methode de deplacement est differente des autres vilains


<Serializable>
Public Class Milice
    Inherits Vilain

    Implements IInfluence

    Public Sub New(pPhoto As Image, pPosition As Point, pDetection As Integer)
        MyBase.New(pPhoto, pPosition, pDetection)
    End Sub



    ''' <summary>
    ''' Permet de changer un citoyen mouton en milice si des conditions sont respecte
    ''' INTERFACE
    ''' </summary>
    ''' <param name="citoyenMouton"></param>
    ''' <returns></returns>
    Public Function InfluenceUnMouton(citoyenMouton As Citoyen) As NonJoueur Implements IInfluence.InfluenceUnMouton
        Dim GardeRoyale As New Milice(Photo, citoyenMouton.Position, 3)
        Return GardeRoyale
    End Function


    ''' <summary>
    ''' Permet de detecter un citoyen si le citoyen se trouve a une distance inferieur a 5 case du vilain
    ''' Permet aussi de s'assurer que le deplacement que la milice fera en suivant le citoyen ne sera pas
    ''' en dehors de la grille
    ''' </summary>
    ''' <param name="listPersonnage"></param>
    ''' <returns></returns>
    Public Function Detectioncitoyen(listPersonnage As List(Of Personnage))
        Dim meilleurDistance As Integer = -1
        For Each pPersonnage In listPersonnage
            If TypeOf pPersonnage Is Citoyen AndAlso CType(pPersonnage, Citoyen).Influencement = False And Position.X > 1 AndAlso Position.X < 551 AndAlso Position.Y > 1 AndAlso Position.Y < 551 Then

                If (Math.Sqrt((Position.X - pPersonnage.Position.X) ^ 2 + (Position.Y - pPersonnage.Position.Y) ^ 2)) / 50 = 0 Then
                    CType(pPersonnage, Citoyen).Influencement = True
                    Return True
                End If

                If (Math.Sqrt((Position.X - pPersonnage.Position.X) ^ 2 + (Position.Y - pPersonnage.Position.Y) ^ 2)) / 50 < 5 Then
                    meilleurDistance = (Math.Sqrt((Position.X - pPersonnage.Position.X) ^ 2 + (Position.Y - pPersonnage.Position.Y) ^ 2)) / 50

                    If (Math.Sqrt((Position.X + 50 - pPersonnage.Position.X) ^ 2 + (Position.Y - pPersonnage.Position.Y) ^ 2)) / 50 < meilleurDistance Then
                        Position = New Point(Position.X + 50, Position.Y)
                        meilleurDistance = Math.Sqrt((Position.X + 50 - pPersonnage.Position.X) ^ 2 + (Position.Y - pPersonnage.Position.Y) ^ 2) / 50
                    End If

                    If (Math.Sqrt((Position.X - 50 - pPersonnage.Position.X) ^ 2 + (Position.Y - pPersonnage.Position.Y) ^ 2)) / 50 < meilleurDistance Then
                        Position = New Point(Position.X - 50, Position.Y)
                        meilleurDistance = Math.Sqrt((Position.X - 50 - pPersonnage.Position.X) ^ 2 + (Position.Y - pPersonnage.Position.Y) ^ 2) / 50
                    End If

                    If (Math.Sqrt((Position.X - pPersonnage.Position.X) ^ 2 + (Position.Y + 50 - pPersonnage.Position.Y) ^ 2)) / 50 < meilleurDistance Then
                        Position = New Point(Position.X, Position.Y + 50)
                        meilleurDistance = Math.Sqrt((Position.X - pPersonnage.Position.X) ^ 2 + (Position.Y + 50 - pPersonnage.Position.Y) ^ 2) / 50
                    End If
                    If Math.Sqrt(((Position.X - pPersonnage.Position.X) ^ 2 + (Position.Y - 50 - pPersonnage.Position.Y) ^ 2)) / 50 < meilleurDistance Then
                        Position = New Point(Position.X, Position.Y - 50)
                        meilleurDistance = Math.Sqrt((Position.X - pPersonnage.Position.X) ^ 2 + (Position.Y - pPersonnage.Position.Y) ^ 2) / 50
                    End If
                End If
            End If

        Next
        If meilleurDistance = -1 Then
            Return False
        End If

        Return True
    End Function

End Class
