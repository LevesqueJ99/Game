﻿'Fichier Tp2
'
'Classe Patrouilleur
'
'Auteur: Joey Levesque
'
'Utilite: Permet de creer un objet Patrouilleur
'
'Pourquoi cette classe existe? Cette classe se differencie des autres vilain parce que son
'style de deplacement est differend des deux autres vilains

<Serializable>
Public Class Patrouilleur
    Inherits Vilain


    Public Sub New(pPhoto As Image, pPosition As Point, pDetection As Integer)
        MyBase.New(pPhoto, pPosition, pDetection)
    End Sub


    ''' <summary>
    ''' Reecriture de la methode bouger qui permet de faire bouger le patrouilleur deux fois
    ''' par tour d'une maniere aleatoire. Cette methode verifie aussi la position du patrouilleur
    ''' afin de l'empecher de sortir des limites du picturebox
    ''' </summary>
    Public Overrides Sub Bouger()

        For index = 1 To 2
            Dim iNbAleatoire As Integer
            iNbAleatoire = GenererNbAleatoire(0, 3)
            Select Case iNbAleatoire
                Case 0
                    If Position.Y > 1 Then
                        Position = New Point(Position.X, Position.Y - 50)
                    Else
                        index -= 1
                    End If

                Case 1
                    If Position.Y < 551 Then
                        Position = New Point(Position.X, Position.Y + 50)
                    Else
                        index -= 1
                    End If
                Case 2
                    If Position.X > 1 Then
                        Position = New Point(Position.X - 50, Position.Y)
                    Else
                        index -= 1
                    End If
                Case 3
                    If Position.X < 551 Then
                        Position = New Point(Position.X + 50, Position.Y)
                    Else
                        index -= 1
                    End If
            End Select
        Next
    End Sub

End Class
