﻿'Fichier Tp2
'
'Classe Vilain
'
'Auteur: Joey Levesque
'
'Utilite: Permet de creer un objet must-inherit Vilain
'
'Pourquoi existe-t-il?: Parce que toutes les autres classes enfants(Patrouilleur,milice,douanier)
'ont besoin d'
<Serializable>
Public MustInherit Class Vilain
    Inherits NonJoueur

    Private _Detection As Integer

#Region "Get/Set"
    Public Property Detection() As Integer
        Get
            Return _Detection
        End Get
        Set(ByVal value As Integer)
            _Detection = value
        End Set
    End Property
#End Region

    Protected Sub New(pPhoto As Image, pPosition As Point, pDetection As Integer)
        MyBase.New(pPhoto, pPosition)
        Detection = pDetection
    End Sub
    ''' <summary>
    ''' Permet avec la listDesPersonnage passe en parametre de determiner si un pPersonnage
    ''' du type Joueur ou FauxRefugie se trouve a une distance plus petite que la detection du 
    ''' Vilain. Par la suite, nous verifions si augmente de une case ou de diminuer d'une case 
    ''' rapproche le vilain du poursuivi et, si c'est le cas, un mouvement sera fait dans cette direction.
    ''' 
    ''' tl;dr: Pythagore
    ''' </summary>
    ''' <param name="listPersonnage"></param>
    ''' <returns></returns>
    Public Function DetectionRefugie(listPersonnage As List(Of Personnage)) As Boolean
        Dim meilleurDistance As Integer = -1
        For Each pPersonnage In listPersonnage
            If TypeOf pPersonnage Is Joueur OrElse TypeOf pPersonnage Is FauxRefugie Then
                If (Math.Sqrt((Position.X - pPersonnage.Position.X) ^ 2 + (Position.Y - pPersonnage.Position.Y) ^ 2)) / 50 = 0 Then
                    Return True
                End If
                If (Math.Sqrt((Position.X - pPersonnage.Position.X) ^ 2 + (Position.Y - pPersonnage.Position.Y) ^ 2)) / 50 < Detection Then
                    meilleurDistance = (Math.Sqrt((Position.X - pPersonnage.Position.X) ^ 2 + (Position.Y - pPersonnage.Position.Y) ^ 2)) / 50

                    If (Math.Sqrt((Position.X + 50 - pPersonnage.Position.X) ^ 2 + (Position.Y - pPersonnage.Position.Y) ^ 2)) / 50 < meilleurDistance Then
                        Position = New Point(Position.X + 50, Position.Y)
                        meilleurDistance = Math.Sqrt((Position.X + 50 - pPersonnage.Position.X) ^ 2 + (Position.Y - pPersonnage.Position.Y) ^ 2) / 50
                    End If

                    If (Math.Sqrt((Position.X - 50 - pPersonnage.Position.X) ^ 2 + (Position.Y - pPersonnage.Position.Y) ^ 2)) / 50 < meilleurDistance Then
                        Position = New Point(Position.X - 50, Position.Y)
                        meilleurDistance = Math.Sqrt((Position.X - 50 - pPersonnage.Position.X) ^ 2 + (Position.Y - pPersonnage.Position.Y) ^ 2) / 50
                    End If

                    If (Math.Sqrt((Position.X - pPersonnage.Position.X) ^ 2 + (Position.Y + 50 - pPersonnage.Position.Y) ^ 2)) / 50 < meilleurDistance Then
                        Position = New Point(Position.X, Position.Y + 50)
                        meilleurDistance = Math.Sqrt((Position.X - pPersonnage.Position.X) ^ 2 + (Position.Y + 50 - pPersonnage.Position.Y) ^ 2) / 50
                    End If
                    If Math.Sqrt(((Position.X - pPersonnage.Position.X) ^ 2 + (Position.Y - 50 - pPersonnage.Position.Y) ^ 2)) / 50 < meilleurDistance Then
                        Position = New Point(Position.X, Position.Y - 50)
                        meilleurDistance = Math.Sqrt((Position.X - pPersonnage.Position.X) ^ 2 + (Position.Y - pPersonnage.Position.Y) ^ 2) / 50
                    End If
                End If
            End If

        Next
        If meilleurDistance = -1 Then
            Return False
        End If

        Return True
    End Function

End Class
