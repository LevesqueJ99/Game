﻿'Fichier Tp2
'
'Classe: Joueur
'
'Auteur: Joey Levesque
'
'Utilite:Permet de creer un objet Joueur.
'
'Pourquoi cette classe existe? Cette classe existe et se demarque par le fait qu'il
'n'y a pas d'algorithme derriere son deplacement contrairement au non-joueur. Deplus, il fallait 
'etre en mesure de permettre aux joueurs de modifier un citoyen en faux-refugies donc il fallait
'lui implanter l'interface IInfluence

<Serializable>
Public Class Joueur
    Inherits Personnage
    Implements IInfluence

    Public Sub New(pPhoto As Image, pPosition As Point)
        MyBase.New(pPhoto, pPosition)
    End Sub

    ''' <summary>
    ''' Permet de transformer un citoyenMouton en faux-refugie
    ''' Il s'agit d'une interface
    ''' </summary>
    ''' <param name="citoyenMouton"></param>
    ''' <returns></returns>
    Public Function IInfluence_InfluenceUnMouton(citoyenMouton As Citoyen) As NonJoueur Implements IInfluence.InfluenceUnMouton
        Dim imageFauxRefugie As Image
        Dim ffauxRefugie As FauxRefugie
        Try
            imageFauxRefugie = (Image.FromFile("../../Images/jeynePoole.jpg"))
            ffauxRefugie = New FauxRefugie(imageFauxRefugie, citoyenMouton.Position)
        Catch
            ffauxRefugie = New FauxRefugie(Photo, citoyenMouton.Position)
        End Try

        Return ffauxRefugie
    End Function


End Class
