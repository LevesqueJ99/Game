﻿'Fichier: Tp2
'
'Classe: Personnage
'
'Auteur:Joey Levesque
'
'Utilite: Permet de creer un objet Personnage
'
'Pourquoi cette classe existe? Parce que toutes les classes enfants ont les attributs
'Photo ainsi que point. Aussi tout les personnages ont en commun la methode Afficher



<Serializable>
Public MustInherit Class Personnage
    Private _Photo As Image
    Private _Position As Point

#Region "Get/Set"
    Public Property Position() As Point
        Get
            Return _Position
        End Get
        Set(ByVal value As Point)
            _Position = value
        End Set
    End Property
    Public Property Photo() As Image
        Get
            Return _Photo
        End Get
        Set(ByVal value As Image)
            _Photo = value
        End Set
    End Property
#End Region

    Protected Sub New(pPhoto As Image, pPosition As Point)
        Photo = pPhoto
        Position = pPosition
    End Sub

    ''' <summary>
    ''' Cette methode permet de faire afficher l'image du personnage a la position 
    ''' du personnage. Une verification se fait avant de dessiner afin d'empecher de dessiner les citoyens
    ''' de sortir du graphic
    ''' </summary>
    ''' <param name="g"></param>
    Public Sub Afficher(g As Graphics)
        If Position.X < 551 OrElse Position.X > 1 OrElse Position.Y < 551 OrElse Position.Y > 1 Then
            g.DrawImage(Photo, Position.X, Position.Y)
        End If
    End Sub

End Class
