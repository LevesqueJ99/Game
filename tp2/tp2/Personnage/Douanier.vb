﻿'Fichier: Tp2
'
'Classe: Douanier
'
'Utilite: Permet de creer un Douanier
'
'Pourquoi cette classe existe? Cette classe existe et se differencie des autres vilains parce
'que son deplacement se fait deux fois de facon aleatoire ce qui est differend des autres vilains


<Serializable>
Public Class Douanier
    Inherits Vilain

    Public Sub New(pPhoto As Image, pPosition As Point, pDetection As Integer)
        MyBase.New(pPhoto, pPosition, pDetection)
    End Sub


    ''' <summary>
    ''' Cette methode bouger permet de faire bouger le douanier dans un chemin 
    ''' predetermine (rectangle) en modifiant sa position 
    ''' </summary>

    Public Overrides Sub Bouger()

        If Position.X > 101 AndAlso Position.Y > 450 Then
            Position = New Point(Position.X - 50, Position.Y)
        ElseIf Position.X < 452 AndAlso Position.Y > 102 Then
            Position = New Point(Position.X, Position.Y - 50)
        ElseIf Position.X < 452 AndAlso Position.Y < 102 Then
            Position = New Point(Position.X + 50, Position.Y)
        Else
            Position = New Point(Position.X, Position.Y + 50)
        End If

    End Sub

End Class
