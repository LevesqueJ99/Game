﻿'Fichier Utilitaires
'
'Classe Utilitaire
'
'Auteur: Joey Levesque
'
'Cette classe contient un Enum qui est utile pour les citoyens et elle contient aussi une methode permettant
'de retourner un nombre pseudo-aleatoire compris entre deux autres nombres


Public Enum Direction
    Est
    SudEst
    Sud
    SudOuest
    Ouest
    NordOuest
    Nord
    NordEst
End Enum

Public Module Utilitaire

    Private random = Nothing
    ''' <summary>
    ''' Cette methode permet d'obtenir un nombre pseudo-aleatoire
    ''' compris entre les deux nombres obtenues en parametres
    ''' </summary>
    ''' <param name="Min"></param>
    ''' <param name="Max"></param>
    ''' <returns>un nombre int compris entre min et max</returns>
    Public Function GenererNbAleatoire(Min As Integer, Max As Integer) As Integer

        If (random Is Nothing) Then
            random = New Random
        End If

        Dim nombreAleatoire = random.Next(Min, Max + 1)

        Return nombreAleatoire

    End Function

End Module
