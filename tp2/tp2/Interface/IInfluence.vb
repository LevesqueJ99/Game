﻿'Fichier:Tp2
'
'Interface: Influencer
'
'Auteur: Joey Levesque
'
'Utilite: Cette interface qui sera utilise dans les classes milices
'et Joueur permet de modifier un citoyen en une milice ou un fauxRefugies
'

Public Interface IInfluence
    'Méthode pour modifier un citoyen mouton en faux-réfugié
    Function InfluenceUnMouton(citoyenMouton As Citoyen) As NonJoueur

End Interface

