﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmBravo
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblFelicitation = New System.Windows.Forms.Label()
        Me.lblVotreNom = New System.Windows.Forms.Label()
        Me.txtNom = New System.Windows.Forms.TextBox()
        Me.btnOui = New System.Windows.Forms.Button()
        Me.Lbl_ErreurNom = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblFelicitation
        '
        Me.lblFelicitation.AutoSize = True
        Me.lblFelicitation.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFelicitation.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lblFelicitation.Location = New System.Drawing.Point(68, 33)
        Me.lblFelicitation.Name = "lblFelicitation"
        Me.lblFelicitation.Size = New System.Drawing.Size(151, 24)
        Me.lblFelicitation.TabIndex = 4
        Me.lblFelicitation.Text = "Congratulation!"
        '
        'lblVotreNom
        '
        Me.lblVotreNom.AutoSize = True
        Me.lblVotreNom.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVotreNom.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lblVotreNom.Location = New System.Drawing.Point(86, 87)
        Me.lblVotreNom.Name = "lblVotreNom"
        Me.lblVotreNom.Size = New System.Drawing.Size(95, 20)
        Me.lblVotreNom.TabIndex = 5
        Me.lblVotreNom.Text = "Your name :"
        '
        'txtNom
        '
        Me.txtNom.Location = New System.Drawing.Point(53, 109)
        Me.txtNom.MaxLength = 10
        Me.txtNom.Name = "txtNom"
        Me.txtNom.Size = New System.Drawing.Size(179, 20)
        Me.txtNom.TabIndex = 6
        '
        'btnOui
        '
        Me.btnOui.BackColor = System.Drawing.SystemColors.Menu
        Me.btnOui.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOui.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOui.Location = New System.Drawing.Point(107, 158)
        Me.btnOui.Name = "btnOui"
        Me.btnOui.Size = New System.Drawing.Size(74, 27)
        Me.btnOui.TabIndex = 7
        Me.btnOui.Text = "&OK"
        Me.btnOui.UseVisualStyleBackColor = False
        '
        'Lbl_ErreurNom
        '
        Me.Lbl_ErreurNom.AutoSize = True
        Me.Lbl_ErreurNom.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Lbl_ErreurNom.Location = New System.Drawing.Point(87, 132)
        Me.Lbl_ErreurNom.Name = "Lbl_ErreurNom"
        Me.Lbl_ErreurNom.Size = New System.Drawing.Size(106, 13)
        Me.Lbl_ErreurNom.TabIndex = 8
        Me.Lbl_ErreurNom.Text = "Entrez un nom valide"
        Me.Lbl_ErreurNom.Visible = False
        '
        'FrmBravo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.ClientSize = New System.Drawing.Size(284, 197)
        Me.Controls.Add(Me.Lbl_ErreurNom)
        Me.Controls.Add(Me.btnOui)
        Me.Controls.Add(Me.txtNom)
        Me.Controls.Add(Me.lblVotreNom)
        Me.Controls.Add(Me.lblFelicitation)
        Me.Name = "FrmBravo"
        Me.Text = "You win!"
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

    Friend WithEvents lblFelicitation As Label
    Friend WithEvents lblVotreNom As Label
    Friend WithEvents txtNom As TextBox
    Friend WithEvents btnOui As Button
    Friend WithEvents Lbl_ErreurNom As Label
End Class
