﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMenu
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMenu))
        Me.btnJouer = New System.Windows.Forms.Button()
        Me.btnHighScores = New System.Windows.Forms.Button()
        Me.btnQuitter = New System.Windows.Forms.Button()
        Me.Lbl_Erreur = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnJouer
        '
        Me.btnJouer.BackColor = System.Drawing.SystemColors.Menu
        Me.btnJouer.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJouer.Location = New System.Drawing.Point(212, 282)
        Me.btnJouer.Name = "btnJouer"
        Me.btnJouer.Size = New System.Drawing.Size(134, 38)
        Me.btnJouer.TabIndex = 0
        Me.btnJouer.Text = "&Play"
        Me.btnJouer.UseVisualStyleBackColor = False
        '
        'btnHighScores
        '
        Me.btnHighScores.BackColor = System.Drawing.SystemColors.Menu
        Me.btnHighScores.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHighScores.Location = New System.Drawing.Point(212, 326)
        Me.btnHighScores.Name = "btnHighScores"
        Me.btnHighScores.Size = New System.Drawing.Size(134, 38)
        Me.btnHighScores.TabIndex = 1
        Me.btnHighScores.Text = "&Highscores"
        Me.btnHighScores.UseVisualStyleBackColor = False
        '
        'btnQuitter
        '
        Me.btnQuitter.BackColor = System.Drawing.SystemColors.Menu
        Me.btnQuitter.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnQuitter.Location = New System.Drawing.Point(212, 370)
        Me.btnQuitter.Name = "btnQuitter"
        Me.btnQuitter.Size = New System.Drawing.Size(134, 38)
        Me.btnQuitter.TabIndex = 2
        Me.btnQuitter.Text = "&Quit"
        Me.btnQuitter.UseVisualStyleBackColor = False
        '
        'Lbl_Erreur
        '
        Me.Lbl_Erreur.AutoSize = True
        Me.Lbl_Erreur.ForeColor = System.Drawing.Color.Red
        Me.Lbl_Erreur.Location = New System.Drawing.Point(352, 282)
        Me.Lbl_Erreur.Name = "Lbl_Erreur"
        Me.Lbl_Erreur.Size = New System.Drawing.Size(93, 13)
        Me.Lbl_Erreur.TabIndex = 3
        Me.Lbl_Erreur.Text = "Image Manquante"
        Me.Lbl_Erreur.Visible = False
        '
        'FrmMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(574, 561)
        Me.Controls.Add(Me.Lbl_Erreur)
        Me.Controls.Add(Me.btnQuitter)
        Me.Controls.Add(Me.btnHighScores)
        Me.Controls.Add(Me.btnJouer)
        Me.Name = "FrmMenu"
        Me.Text = "The Key to the North"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnJouer As Button
    Friend WithEvents btnHighScores As Button
    Friend WithEvents btnQuitter As Button
    Friend WithEvents Lbl_Erreur As Label
End Class
