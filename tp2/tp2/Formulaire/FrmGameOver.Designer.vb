﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGameOver
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnOui = New System.Windows.Forms.Button()
        Me.lblRejouer = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnOui
        '
        Me.btnOui.BackColor = System.Drawing.SystemColors.Menu
        Me.btnOui.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOui.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOui.Location = New System.Drawing.Point(97, 64)
        Me.btnOui.Name = "btnOui"
        Me.btnOui.Size = New System.Drawing.Size(77, 27)
        Me.btnOui.TabIndex = 1
        Me.btnOui.Text = "&OK"
        Me.btnOui.UseVisualStyleBackColor = False
        '
        'lblRejouer
        '
        Me.lblRejouer.AutoSize = True
        Me.lblRejouer.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRejouer.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lblRejouer.Location = New System.Drawing.Point(93, 19)
        Me.lblRejouer.Name = "lblRejouer"
        Me.lblRejouer.Size = New System.Drawing.Size(99, 20)
        Me.lblRejouer.TabIndex = 3
        Me.lblRejouer.Text = "Game Over"
        '
        'FrmGameOver
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(284, 103)
        Me.Controls.Add(Me.lblRejouer)
        Me.Controls.Add(Me.btnOui)
        Me.Name = "FrmGameOver"
        Me.Text = "Game Over"
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

    Friend WithEvents btnOui As Button
    Friend WithEvents lblRejouer As Label
End Class
