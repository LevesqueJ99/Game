﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmGrilleDeJeu
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmGrilleDeJeu))
        Me.pbGrille = New System.Windows.Forms.PictureBox()
        Me.Lbl_nbDeplacement = New System.Windows.Forms.Label()
        CType(Me.pbGrille, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pbGrille
        '
        Me.pbGrille.BackColor = System.Drawing.Color.SlateGray
        Me.pbGrille.Location = New System.Drawing.Point(11, 10)
        Me.pbGrille.Name = "pbGrille"
        Me.pbGrille.Size = New System.Drawing.Size(600, 600)
        Me.pbGrille.TabIndex = 0
        Me.pbGrille.TabStop = False
        '
        'Lbl_nbDeplacement
        '
        Me.Lbl_nbDeplacement.AutoSize = True
        Me.Lbl_nbDeplacement.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Lbl_nbDeplacement.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Lbl_nbDeplacement.Location = New System.Drawing.Point(705, 10)
        Me.Lbl_nbDeplacement.Name = "Lbl_nbDeplacement"
        Me.Lbl_nbDeplacement.Size = New System.Drawing.Size(13, 13)
        Me.Lbl_nbDeplacement.TabIndex = 1
        Me.Lbl_nbDeplacement.Text = "0"
        '
        'FrmGrilleDeJeu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(924, 621)
        Me.Controls.Add(Me.Lbl_nbDeplacement)
        Me.Controls.Add(Me.pbGrille)
        Me.Name = "FrmGrilleDeJeu"
        Me.Text = "Game"
        CType(Me.pbGrille, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pbGrille As PictureBox
    Friend WithEvents Lbl_nbDeplacement As Label
End Class
