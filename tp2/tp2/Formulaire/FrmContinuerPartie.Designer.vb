﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmContinuerPartie
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblContinuer = New System.Windows.Forms.Label()
        Me.btnNon = New System.Windows.Forms.Button()
        Me.btnOui = New System.Windows.Forms.Button()
        Me.SuspendLayout
        '
        'lblContinuer
        '
        Me.lblContinuer.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContinuer.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lblContinuer.Location = New System.Drawing.Point(26, 9)
        Me.lblContinuer.Name = "lblContinuer"
        Me.lblContinuer.Size = New System.Drawing.Size(233, 47)
        Me.lblContinuer.TabIndex = 6
        Me.lblContinuer.Text = "You left a game, do you want to continue?"
        Me.lblContinuer.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'btnNon
        '
        Me.btnNon.BackColor = System.Drawing.SystemColors.Menu
        Me.btnNon.DialogResult = System.Windows.Forms.DialogResult.No
        Me.btnNon.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNon.Location = New System.Drawing.Point(145, 80)
        Me.btnNon.Name = "btnNon"
        Me.btnNon.Size = New System.Drawing.Size(74, 27)
        Me.btnNon.TabIndex = 8
        Me.btnNon.Text = "&No"
        Me.btnNon.UseVisualStyleBackColor = False
        '
        'btnOui
        '
        Me.btnOui.BackColor = System.Drawing.SystemColors.Menu
        Me.btnOui.DialogResult = System.Windows.Forms.DialogResult.Yes
        Me.btnOui.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOui.Location = New System.Drawing.Point(65, 80)
        Me.btnOui.Name = "btnOui"
        Me.btnOui.Size = New System.Drawing.Size(74, 27)
        Me.btnOui.TabIndex = 7
        Me.btnOui.Text = "&Yes"
        Me.btnOui.UseVisualStyleBackColor = False
        '
        'ContinuerPartie
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.ClientSize = New System.Drawing.Size(297, 132)
        Me.Controls.Add(Me.btnNon)
        Me.Controls.Add(Me.btnOui)
        Me.Controls.Add(Me.lblContinuer)
        Me.Name = "ContinuerPartie"
        Me.Text = "Continue?"
        Me.ResumeLayout(false)

End Sub

    Friend WithEvents lblContinuer As Label
    Friend WithEvents btnNon As Button
    Friend WithEvents btnOui As Button
End Class
