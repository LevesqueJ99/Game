﻿Imports System.IO
Public Class FrmGrilleDeJeu
    Private listPersonnage As New List(Of Personnage)
    Private i As Integer = 0
    Private nbDeplacement As Integer
    Private jJeu As Jeu
    Private imageCitoyen As Image
    Private imageFauxRefugie As Image

    Public Sub New(Jeu As Jeu)
        InitializeComponent()
        jJeu = Jeu
        listPersonnage = jJeu.ListePersonnage
        nbDeplacement = jJeu.nbDeplacement
    End Sub

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If listPersonnage.Count = 0 Then

            Try
                listPersonnage.Add(New Joueur((Image.FromFile("../../Images/aryaStark.jpg")), New Point(1, 1)))
                listPersonnage.Add(New Douanier((Image.FromFile("../../Images/cerseiLannister.jpg")), New Point(501, 501), 5))
                listPersonnage.Add(New Milice((Image.FromFile("../../Images/merynTrant.jpg")), New Point(501, 101), 3))
                listPersonnage.Add(New Patrouilleur((Image.FromFile("../../Images/sandorClegane.jpg")), New Point(301, 501), 4))
                imageCitoyen = Image.FromFile("../../Images/paysan.jpg")
                nbDeplacement = 0
            Catch ex As FileNotFoundException
                DialogResult = DialogResult.Retry
            End Try
            NouveauCitoyen()
        End If
        AddHandler pbGrille.Paint, AddressOf pbGrille_Paint
        Controls.Add(pbGrille)
    End Sub

    ''' <summary>
    ''' Permet de dessiner dans le pictrubox pbGrille
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub pbGrille_Paint(ByVal sender As Object, ByVal e As PaintEventArgs)
        Dim VariableTempo As Integer
        For index = 0 To 12
            VariableTempo = index * 50
            e.Graphics.DrawLine(System.Drawing.Pens.AntiqueWhite, VariableTempo, 0, VariableTempo, 600)
        Next
        For index = 0 To 12
            VariableTempo = index * 50
            e.Graphics.DrawLine(System.Drawing.Pens.AntiqueWhite, 0, VariableTempo, 600, VariableTempo)
        Next

        For Each elem In listPersonnage
            elem.Afficher(e.Graphics)
        Next
        Lbl_nbDeplacement.Text = nbDeplacement
        jJeu.nbDeplacement = nbDeplacement
    End Sub

    ''' <summary>
    ''' Permet de modifier la position du joueur en fonction des fleches appuyes
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub Form1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Select Case e.KeyCode
            Case Keys.Up
                If (listPersonnage(0).Position.Y > 1) Then
                    listPersonnage(0).Position = New Point(listPersonnage(0).Position.X, listPersonnage(0).Position.Y - 50)
                    nbDeplacement += 1
                    GestionDesMouvements()
                End If
            Case Keys.Down
                If (listPersonnage(0).Position.Y < 550) Then
                    listPersonnage(0).Position = New Point(listPersonnage(0).Position.X, listPersonnage(0).Position.Y + 50)
                    nbDeplacement += 1
                    GestionDesMouvements()
                End If
            Case Keys.Left
                If (listPersonnage(0).Position.X > 2) Then
                    listPersonnage(0).Position = New Point(listPersonnage(0).Position.X - 50, listPersonnage(0).Position.Y)
                    nbDeplacement += 1
                    GestionDesMouvements()
                End If
            Case Keys.Right
                If (listPersonnage(0).Position.X < 550) Then
                    listPersonnage(0).Position = New Point(listPersonnage(0).Position.X + 50, listPersonnage(0).Position.Y)
                    nbDeplacement += 1
                    GestionDesMouvements()
                End If
        End Select
    End Sub

    ''' <summary>
    ''' Permet de determiner si le vilain bouge selon algorithme ou s'il 
    ''' essait de se rapprocher du joueur
    ''' </summary>
    Private Sub GestionDesMouvements()
        For index = 1 To listPersonnage.Count - 1
            If TypeOf listPersonnage(index) Is Vilain AndAlso CType(listPersonnage(index), Vilain).DetectionRefugie(listPersonnage) = True Then

            ElseIf TypeOf listPersonnage(index) Is Milice AndAlso CType(listPersonnage(index), Milice).Detectioncitoyen(listPersonnage) = True Then

            ElseIf TypeOf listPersonnage(index) Is Vilain AndAlso CType(listPersonnage(index), Vilain).DetectionRefugie(listPersonnage) = False Then
                CType(listPersonnage(index), NonJoueur).Bouger()
            ElseIf TypeOf listPersonnage(index) Is Citoyen AndAlso nbDeplacement Mod 2 = 0 Then
                CType(listPersonnage(index), Citoyen).nbDeplacementJeu = nbDeplacement
                CType(listPersonnage(index), NonJoueur).Bouger()
            ElseIf TypeOf listPersonnage(index) IsNot Citoyen Then
                CType(listPersonnage(index), NonJoueur).Bouger()
            End If

        Next
        NouveauCitoyen()
        Refresh()
        ValidationCollision()

    End Sub

    ''' <summary>
    ''' Permet de savoir si 2 personnages sont sur la meme case
    ''' dependamment des personnages sur la meme case une action approprie sera faite
    ''' </summary>
    Private Sub ValidationCollision()

        Dim index As Integer = 0
        Dim iLongueurListe = listPersonnage.Count - 1
        While (index < iLongueurListe)
            If TypeOf (listPersonnage(index)) Is Vilain AndAlso listPersonnage(index).Position = listPersonnage(0).Position Then
                If (nbDeplacement > jJeu.HighScore(4)) Then
                    Dim formNouveauRecord As New FrmBravo(jJeu, nbDeplacement)
                    formNouveauRecord.ShowDialog()
                    DialogResult = DialogResult.OK
                ElseIf DialogResult <> DialogResult.OK Then
                    Dim formGameOver As New FrmGameOver
                    formGameOver.ShowDialog()
                    DialogResult = DialogResult.OK

                End If
            End If
            If TypeOf listPersonnage(index) Is Citoyen AndAlso listPersonnage(index).Position = listPersonnage(0).Position AndAlso CType(listPersonnage(index), Citoyen).Mouton = True Then
                Dim fauxRefugie As FauxRefugie = CType(listPersonnage(0), Joueur).IInfluence_InfluenceUnMouton(listPersonnage(index))
                listPersonnage.Insert(index, fauxRefugie)
                listPersonnage.RemoveAt(index + 1)
            End If
            For compteur = 0 To iLongueurListe - 1
                If TypeOf (listPersonnage(index)) Is Citoyen AndAlso TypeOf listPersonnage(compteur) Is Milice AndAlso listPersonnage(index).Position = listPersonnage(compteur).Position AndAlso CType(listPersonnage(index), Citoyen).Mouton Then
                    Dim Milice As Milice = CType(listPersonnage(compteur), Milice).InfluenceUnMouton(listPersonnage(index))
                    listPersonnage.Insert(index, Milice)
                    listPersonnage.RemoveAt(index + 1)

                ElseIf TypeOf (listPersonnage(index)) Is FauxRefugie AndAlso TypeOf listPersonnage(compteur) Is Vilain AndAlso listPersonnage(index).Position = listPersonnage(compteur).Position Then
                    listPersonnage.RemoveAt(index)
                    iLongueurListe -= 1
                End If
            Next
            If TypeOf listPersonnage(index) Is Citoyen AndAlso listPersonnage(index).Position.X > 601 OrElse listPersonnage(index).Position.Y > 601 OrElse listPersonnage(index).Position.Y < 1 OrElse listPersonnage(index).Position.X < 1 Then
                listPersonnage.RemoveAt(index)
                iLongueurListe -= 1
            End If

            index += 1
            'Next
        End While
    End Sub

    ''' <summary>
    ''' Permet de creer un nouveau citoyen
    ''' </summary>
    Private Sub NouveauCitoyen()
        If nbDeplacement Mod 5 = 0 OrElse nbDeplacement = 0 Then
            Dim iNbAleatoire As Integer = GenererNbAleatoire(0, 3)
            Dim pPoint As Point
            Select Case iNbAleatoire
                Case 0
                    pPoint = New Point(1, GenererNbAleatoire(0, 11) * 50 + 1)
                Case 1
                    pPoint = New Point(551, GenererNbAleatoire(0, 11) * 50 + 1)
                Case 2
                    pPoint = New Point(GenererNbAleatoire(0, 11) * 50 + 1, 1)
                Case 3
                    pPoint = New Point(GenererNbAleatoire(0, 11) * 50 + 1, 551)
            End Select
            Try
                listPersonnage.Add(New Citoyen((Image.FromFile("../../Images/paysan.jpg")), pPoint, DeterminerDirection(pPoint)))
            Catch ex As FileNotFoundException
                listPersonnage.Add(New Citoyen(imageCitoyen, pPoint, DeterminerDirection(pPoint)))
            End Try
        End If
    End Sub

    ''' <summary>
    ''' Permet de determiner la direction du citoyen.
    ''' Tres Peu esthetique mais permet de determine la direction d'un citoyen
    ''' </summary>
    ''' <returns></returns>
    Private Function DeterminerDirection(pPoint As Point) As Direction
        Dim dDirection As Direction
        If pPoint.X = 1 AndAlso pPoint.Y = 1 Then
            dDirection = GenererNbAleatoire(0, 2)
        End If
        If pPoint.X > 1 AndAlso pPoint.X < 551 AndAlso pPoint.Y = 1 Then
            dDirection = GenererNbAleatoire(1, 3)
        End If
        If pPoint.Y = 1 AndAlso pPoint.X = 551 Then
            dDirection = GenererNbAleatoire(2, 4)
        End If
        If pPoint.Y > 1 AndAlso pPoint.Y <> 551 AndAlso pPoint.X = 551 Then
            dDirection = GenererNbAleatoire(3, 5)
        End If
        If pPoint.Y = 551 AndAlso pPoint.X = 551 Then
            dDirection = GenererNbAleatoire(4, 6)
        End If
        If pPoint.Y = 551 AndAlso pPoint.X > 1 AndAlso pPoint.X <> 551 Then
            dDirection = GenererNbAleatoire(5, 7)
        End If
        If pPoint.Y = 551 AndAlso pPoint.X = 1 Then
            dDirection = GenererNbAleatoire(6, 8)
        End If
        If pPoint.Y > 1 AndAlso pPoint.X = 1 AndAlso pPoint.Y <> 551 Then
            dDirection = GenererNbAleatoire(7, 9)
        End If
        If dDirection >= 8 Then
            dDirection -= 8
        End If
        Return dDirection
    End Function



End Class