﻿Imports tp2

Public Class FrmHighScores
    Private jeu As Jeu

    Public Sub New(jeu As Jeu)
        InitializeComponent()
        Me.jeu = jeu
    End Sub

    Private Sub HighScores_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Lbl_Nom1.Text = jeu.NomHighScore(0)
        Lbl_Nom2.Text = jeu.NomHighScore(1)
        Lbl_Nom3.Text = jeu.NomHighScore(2)
        Lbl_Nom4.Text = jeu.NomHighScore(3)
        Lbl_Nom5.Text = jeu.NomHighScore(4)
        Lbl_HighScore1.Text = jeu.HighScore(0)
        Lbl_HighScore2.Text = jeu.HighScore(1)
        Lbl_HighScore3.Text = jeu.HighScore(2)
        Lbl_HighScore4.Text = jeu.HighScore(3)
        Lbl_HighScore5.Text = jeu.HighScore(4)

    End Sub
End Class