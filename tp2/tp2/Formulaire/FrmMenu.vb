﻿Imports System.Runtime.Serialization.Formatters.Binary
Public Class FrmMenu
    Private Jeu As Jeu

    Private Sub btnQuitter_Click(sender As Object, e As EventArgs) Handles btnQuitter.Click
        Close()
    End Sub

    Private Sub btnHighScores_Click(sender As Object, e As EventArgs) Handles btnHighScores.Click
        Dim formHighScore As New FrmHighScores(Jeu)
        formHighScore.ShowDialog()
    End Sub

    Private Sub btnJouer_Click(sender As Object, e As EventArgs) Handles btnJouer.Click
        Dim formGrilleDeJeu As New FrmGrilleDeJeu(Jeu)
        If (Jeu.ListePersonnage.Count <> 0) Then
            Dim formcontinuerPartie As New FrmContinuerPartie
            formcontinuerPartie.ShowDialog()
            If formcontinuerPartie.DialogResult = DialogResult.No Then
                Jeu.ListePersonnage.Clear()
            End If
        End If
        formGrilleDeJeu.ShowDialog()
        If formGrilleDeJeu.DialogResult = DialogResult.OK Then
            Jeu.ListePersonnage.Clear()
        End If
        If formGrilleDeJeu.DialogResult = DialogResult.Retry Then
            Lbl_Erreur.Visible = True
        Else
            Lbl_Erreur.Visible = False
        End If


    End Sub

    Private Sub Menu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If (IO.File.Exists("..\..\informationUser.DAT")) Then
            Try
                Dim fic As New IO.FileStream("..\..\informationUser.DAT", IO.FileMode.Open)
                Dim binaryReader As New BinaryFormatter
                Jeu = binaryReader.Deserialize(fic)
                fic.Close()
            Catch ex As Exception
            End Try
        Else
            Jeu = New Jeu
        End If
    End Sub

    Private Sub Menu_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing

        Dim sfd As New SaveFileDialog()
        sfd.Filter = "Fichier DAT (*.DAT) |*.dat"
        sfd.DefaultExt = "dat"

        Dim fic As New IO.FileStream("..\..\informationUser.DAT", IO.FileMode.Create)
        Dim bf As New BinaryFormatter
        bf.Serialize(fic, Jeu)
        fic.Close()

    End Sub

End Class
