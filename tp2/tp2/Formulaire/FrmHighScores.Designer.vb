﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmHighScores
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmHighScores))
        Me.Lbl_HighScore3 = New System.Windows.Forms.Label()
        Me.Lbl_Nom3 = New System.Windows.Forms.Label()
        Me.Lbl_HighScore2 = New System.Windows.Forms.Label()
        Me.Lbl_Nom2 = New System.Windows.Forms.Label()
        Me.Lbl_HighScore1 = New System.Windows.Forms.Label()
        Me.Lbl_Nom1 = New System.Windows.Forms.Label()
        Me.Lbl_HighScore5 = New System.Windows.Forms.Label()
        Me.Lbl_Nom5 = New System.Windows.Forms.Label()
        Me.Lbl_HighScore4 = New System.Windows.Forms.Label()
        Me.Lbl_Nom4 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Lbl_HighScore3
        '
        Me.Lbl_HighScore3.AutoSize = True
        Me.Lbl_HighScore3.BackColor = System.Drawing.SystemColors.ButtonShadow
        Me.Lbl_HighScore3.Location = New System.Drawing.Point(190, 182)
        Me.Lbl_HighScore3.Name = "Lbl_HighScore3"
        Me.Lbl_HighScore3.Size = New System.Drawing.Size(93, 13)
        Me.Lbl_HighScore3.TabIndex = 0
        Me.Lbl_HighScore3.Text = "NbDeplacement 3"
        '
        'Lbl_Nom3
        '
        Me.Lbl_Nom3.AutoSize = True
        Me.Lbl_Nom3.BackColor = System.Drawing.SystemColors.ButtonShadow
        Me.Lbl_Nom3.Location = New System.Drawing.Point(32, 182)
        Me.Lbl_Nom3.Name = "Lbl_Nom3"
        Me.Lbl_Nom3.Size = New System.Drawing.Size(91, 13)
        Me.Lbl_Nom3.TabIndex = 1
        Me.Lbl_Nom3.Text = "Nom HighScore 3"
        '
        'Lbl_HighScore2
        '
        Me.Lbl_HighScore2.AutoSize = True
        Me.Lbl_HighScore2.BackColor = System.Drawing.SystemColors.ButtonShadow
        Me.Lbl_HighScore2.Location = New System.Drawing.Point(190, 141)
        Me.Lbl_HighScore2.Name = "Lbl_HighScore2"
        Me.Lbl_HighScore2.Size = New System.Drawing.Size(93, 13)
        Me.Lbl_HighScore2.TabIndex = 2
        Me.Lbl_HighScore2.Text = "NbDeplacement 2"
        '
        'Lbl_Nom2
        '
        Me.Lbl_Nom2.AutoSize = True
        Me.Lbl_Nom2.BackColor = System.Drawing.SystemColors.ButtonShadow
        Me.Lbl_Nom2.Location = New System.Drawing.Point(32, 141)
        Me.Lbl_Nom2.Name = "Lbl_Nom2"
        Me.Lbl_Nom2.Size = New System.Drawing.Size(91, 13)
        Me.Lbl_Nom2.TabIndex = 3
        Me.Lbl_Nom2.Text = "Nom HighScore 2"
        '
        'Lbl_HighScore1
        '
        Me.Lbl_HighScore1.AutoSize = True
        Me.Lbl_HighScore1.BackColor = System.Drawing.SystemColors.ButtonShadow
        Me.Lbl_HighScore1.Location = New System.Drawing.Point(190, 101)
        Me.Lbl_HighScore1.Name = "Lbl_HighScore1"
        Me.Lbl_HighScore1.Size = New System.Drawing.Size(93, 13)
        Me.Lbl_HighScore1.TabIndex = 4
        Me.Lbl_HighScore1.Text = "NbDeplacement 1"
        '
        'Lbl_Nom1
        '
        Me.Lbl_Nom1.AutoSize = True
        Me.Lbl_Nom1.BackColor = System.Drawing.SystemColors.ButtonShadow
        Me.Lbl_Nom1.Location = New System.Drawing.Point(32, 101)
        Me.Lbl_Nom1.Name = "Lbl_Nom1"
        Me.Lbl_Nom1.Size = New System.Drawing.Size(91, 13)
        Me.Lbl_Nom1.TabIndex = 5
        Me.Lbl_Nom1.Text = "Nom HighScore 1"
        '
        'Lbl_HighScore5
        '
        Me.Lbl_HighScore5.AutoSize = True
        Me.Lbl_HighScore5.BackColor = System.Drawing.SystemColors.ButtonShadow
        Me.Lbl_HighScore5.Location = New System.Drawing.Point(190, 277)
        Me.Lbl_HighScore5.Name = "Lbl_HighScore5"
        Me.Lbl_HighScore5.Size = New System.Drawing.Size(93, 13)
        Me.Lbl_HighScore5.TabIndex = 6
        Me.Lbl_HighScore5.Text = "NbDeplacement 5"
        '
        'Lbl_Nom5
        '
        Me.Lbl_Nom5.AutoSize = True
        Me.Lbl_Nom5.BackColor = System.Drawing.SystemColors.ButtonShadow
        Me.Lbl_Nom5.Location = New System.Drawing.Point(32, 277)
        Me.Lbl_Nom5.Name = "Lbl_Nom5"
        Me.Lbl_Nom5.Size = New System.Drawing.Size(91, 13)
        Me.Lbl_Nom5.TabIndex = 7
        Me.Lbl_Nom5.Text = "Nom HighScore 5"
        '
        'Lbl_HighScore4
        '
        Me.Lbl_HighScore4.AutoSize = True
        Me.Lbl_HighScore4.BackColor = System.Drawing.SystemColors.ButtonShadow
        Me.Lbl_HighScore4.Location = New System.Drawing.Point(190, 227)
        Me.Lbl_HighScore4.Name = "Lbl_HighScore4"
        Me.Lbl_HighScore4.Size = New System.Drawing.Size(93, 13)
        Me.Lbl_HighScore4.TabIndex = 8
        Me.Lbl_HighScore4.Text = "NbDeplacement 4"
        '
        'Lbl_Nom4
        '
        Me.Lbl_Nom4.AutoSize = True
        Me.Lbl_Nom4.BackColor = System.Drawing.SystemColors.ButtonShadow
        Me.Lbl_Nom4.Location = New System.Drawing.Point(32, 227)
        Me.Lbl_Nom4.Name = "Lbl_Nom4"
        Me.Lbl_Nom4.Size = New System.Drawing.Size(91, 13)
        Me.Lbl_Nom4.TabIndex = 9
        Me.Lbl_Nom4.Text = "Nom HighScore 4"
        '
        'HighScores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(314, 376)
        Me.Controls.Add(Me.Lbl_Nom4)
        Me.Controls.Add(Me.Lbl_HighScore4)
        Me.Controls.Add(Me.Lbl_Nom5)
        Me.Controls.Add(Me.Lbl_HighScore5)
        Me.Controls.Add(Me.Lbl_Nom1)
        Me.Controls.Add(Me.Lbl_HighScore1)
        Me.Controls.Add(Me.Lbl_Nom2)
        Me.Controls.Add(Me.Lbl_HighScore2)
        Me.Controls.Add(Me.Lbl_Nom3)
        Me.Controls.Add(Me.Lbl_HighScore3)
        Me.Name = "HighScores"
        Me.Text = "HighScores"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Lbl_HighScore3 As Label
    Friend WithEvents Lbl_Nom3 As Label
    Friend WithEvents Lbl_HighScore2 As Label
    Friend WithEvents Lbl_Nom2 As Label
    Friend WithEvents Lbl_HighScore1 As Label
    Friend WithEvents Lbl_Nom1 As Label
    Friend WithEvents Lbl_HighScore5 As Label
    Friend WithEvents Lbl_Nom5 As Label
    Friend WithEvents Lbl_HighScore4 As Label
    Friend WithEvents Lbl_Nom4 As Label
End Class
