﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports tp2
Imports System.Drawing

<TestClass()> Public Class TestDouanier

    <TestMethod()> Public Sub CreationDouanier()
        Dim douanierTest As New Douanier(Image.FromFile("../../Images/paysan.jpg"), New Point(0, 0), 5)
        Assert.IsNotNull(douanierTest)

        Assert.AreEqual(douanierTest.Position, New Point(0, 0))
        Assert.AreEqual(douanierTest.Detection, 5)
    End Sub

    <TestMethod> Public Sub DouanierBouger()
        Dim douanierTest As New Douanier(Image.FromFile("../../Images/paysan.jpg"), New Point(11, 311), 5)
        douanierTest.Bouger()
        Assert.AreNotEqual(New Point(0, 0), douanierTest.Position)
        Dim pPointTempo = douanierTest.Position
        douanierTest.Bouger()
        Assert.AreNotEqual(pPointTempo, douanierTest.Position)
    End Sub

End Class