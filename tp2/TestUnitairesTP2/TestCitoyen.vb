﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System.Drawing
Imports tp2

<TestClass()> Public Class TestCitoyen

    <TestMethod()> Public Sub Creation_Citoyen()
        Dim citoyenTest As New Citoyen(Image.FromFile("../../Images/aryaStark.jpg"), New Point(0, 0), Direction.Nord)
        Assert.IsNotNull(citoyenTest)
        Assert.AreEqual(citoyenTest.Position, New Point(0, 0))
        Assert.AreEqual(citoyenTest.Direction, Direction.Nord)
    End Sub

    <TestMethod()> Public Sub bouger_Citoyen()
        Dim CitoyenTest As New Citoyen(Image.FromFile("../../Images/aryaStark.jpg"), New Point(0, 0), Direction.Nord)
        CitoyenTest.nbDeplacementJeu = 4
        CitoyenTest.Bouger()
        Assert.AreNotEqual(New Point(0, 0), CitoyenTest.Position)
        Dim pPointTempo = CitoyenTest.Position
        CitoyenTest.Bouger()
        Assert.AreNotEqual(pPointTempo, CitoyenTest.Position)
    End Sub

End Class