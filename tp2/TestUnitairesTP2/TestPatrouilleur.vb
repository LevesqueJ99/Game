﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports tp2
Imports System.Drawing

<TestClass()> Public Class TestPatrouilleur
    <TestMethod()> Public Sub CreationPatrouilleur()
        Dim PatrouilleurTest As New Patrouilleur(Image.FromFile("../../Images/paysan.jpg"), New Point(0, 0), 5)
        Assert.IsNotNull(PatrouilleurTest)

        Assert.AreEqual(PatrouilleurTest.Position, New Point(0, 0))
        Assert.AreEqual(PatrouilleurTest.Detection, 5)
    End Sub

    <TestMethod> Public Sub PatrouilleurBouger()
        Dim PatrouilleurTest As New Patrouilleur(Image.FromFile("../../Images/paysan.jpg"), New Point(0, 0), 5)
        PatrouilleurTest.Bouger()
        Assert.AreNotEqual(New Point(0, 0), PatrouilleurTest.Position)
        Dim pPointTempo = PatrouilleurTest.Position
        PatrouilleurTest.Bouger()
        Assert.AreNotEqual(pPointTempo, PatrouilleurTest.Position)
    End Sub
End Class