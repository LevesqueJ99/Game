﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports tp2
Imports System.Drawing

<TestClass()> Public Class TestJoueur

    <TestMethod()> Public Sub CreationJoueur()
        Dim JoueurTest As New Joueur(Image.FromFile("../../Images/paysan.jpg"), New Point(0, 0))
        Assert.IsNotNull(JoueurTest)
        Assert.AreEqual(JoueurTest.Position, New Point(0, 0))
    End Sub

    <TestMethod> Public Sub IntefaceInfluencerMouton()
        Dim JoueurTest As New Joueur(Image.FromFile("../../Images/aryaStark.jpg"), New Point(13, 333))
        Dim citoyenMouton As New Citoyen(Image.FromFile("../../Images/paysan.jpg"), New Point(100, 0), Direction.NordOuest)
        citoyenMouton.Mouton = True
        Dim fFauxRefugie As FauxRefugie = JoueurTest.IInfluence_InfluenceUnMouton(citoyenMouton)
        Assert.IsNotNull(JoueurTest)
        Assert.IsNotNull(citoyenMouton)
        Assert.IsNotNull(fFauxRefugie)
        Assert.AreNotEqual(fFauxRefugie, citoyenMouton)
    End Sub

End Class