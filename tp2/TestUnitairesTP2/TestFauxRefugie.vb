﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports tp2
Imports System.Drawing

<TestClass()> Public Class TestFauxRefugie
    <TestMethod()> Public Sub CreationFauxRefugie()
        Dim FauxRefugieTest As New FauxRefugie(Image.FromFile("../../Images/paysan.jpg"), New Point(0, 0))
        Assert.IsNotNull(FauxRefugieTest)

        Assert.AreEqual(FauxRefugieTest.Position, New Point(0, 0))
    End Sub

    <TestMethod> Public Sub FauxRefugieBouger()
        Dim FauxRefugieTest As New FauxRefugie(Image.FromFile("../../Images/paysan.jpg"), New Point(0, 0))
        FauxRefugieTest.Bouger()
        Assert.AreNotEqual(New Point(0, 0), FauxRefugieTest.Position)
        Dim pPointTempo = FauxRefugieTest.Position
        FauxRefugieTest.Bouger()
        Assert.AreNotEqual(pPointTempo, FauxRefugieTest.Position)
    End Sub
End Class