﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports tp2
Imports System.Drawing

<TestClass()> Public Class TestMilice
    <TestMethod()> Public Sub CreationMilice()
        Dim MiliceTest As New Milice(Image.FromFile("../../Images/paysan.jpg"), New Point(0, 0), 5)
        Assert.IsNotNull(MiliceTest)

        Assert.AreEqual(MiliceTest.Position, New Point(0, 0))
        Assert.AreEqual(MiliceTest.Detection, 5)
    End Sub

    <TestMethod> Public Sub MiliceBouger()
        Dim MiliceTest As New Milice(Image.FromFile("../../Images/paysan.jpg"), New Point(0, 0), 5)
        MiliceTest.Bouger()
        Assert.AreNotEqual(New Point(0, 0), MiliceTest.Position)
        Dim pPointTempo = MiliceTest.Position
        MiliceTest.Bouger()
        Assert.AreNotEqual(pPointTempo, MiliceTest.Position)
    End Sub
    <TestMethod> Public Sub MiliceInfluencerCitoyen()
        Dim MiliceTest As New Milice(Image.FromFile("../../Images/aryaStark.jpg"), New Point(13, 333), 18)
        Dim citoyenMouton As New Citoyen(Image.FromFile("../../Images/paysan.jpg"), New Point(100, 0), Direction.NordOuest)
        citoyenMouton.Mouton = True
        Dim mMilice As Milice = MiliceTest.InfluenceUnMouton(citoyenMouton)
        Assert.IsNotNull(MiliceTest)
        Assert.IsNotNull(citoyenMouton)
        Assert.IsNotNull(mMilice)
        Assert.AreNotEqual(mMilice, citoyenMouton)
    End Sub

    <TestMethod> Public Sub MiliceDetecterCitoyen()
        Dim MiliceTest As New Milice(Image.FromFile("../../Images/aryaStark.jpg"), New Point(13, 333), 18)
        Dim listPersonnage As New List(Of Personnage)
        listPersonnage.Add(New Citoyen(Image.FromFile("../../Images/aryaStark.jpg"), New Point(63, 333), 3))
        MiliceTest.Detectioncitoyen(listPersonnage)
        Assert.IsNotNull(MiliceTest)
        Assert.AreNotEqual(MiliceTest.Position, New Point(13, 333))
        Assert.AreEqual(MiliceTest.Position, listPersonnage(0).Position)
    End Sub
End Class