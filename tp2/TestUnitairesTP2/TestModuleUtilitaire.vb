﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports tp2

<TestClass()> Public Class TestModuleUtilitaire

    <TestMethod()> Public Sub TestModuleUtilitaire()
        Dim iNb1 As Integer = GenererNbAleatoire(3, 17)
        Dim iNb2 As Integer = GenererNbAleatoire(18, 145)

        Assert.IsNotNull(iNb1)
        Assert.IsNotNull(iNb2)
        Assert.AreNotEqual(iNb1, iNb2)
    End Sub

End Class