﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports tp2
Imports System.Drawing

<TestClass()> Public Class TestVilain

    <TestMethod()> Public Sub TestDetecterRefugie()
        Dim douanierTest As New Douanier(Image.FromFile("../../Images/aryaStark.jpg"), New Point(63, 333), 18)
        Dim listPersonnage As New List(Of Personnage)
        listPersonnage.Add(New Joueur(Image.FromFile("../../Images/aryaStark.jpg"), New Point(63, 333)))
        douanierTest.DetectionRefugie(listPersonnage)
        Assert.IsNotNull(douanierTest)
        Assert.AreEqual(douanierTest.Position, listPersonnage(0).Position)
        douanierTest.Position = New Point(13, 333)
        douanierTest.DetectionRefugie(listPersonnage)
        Assert.AreEqual(listPersonnage(0).Position.X, douanierTest.Position.X)
        Assert.AreEqual(douanierTest.Position.Y, listPersonnage(0).Position.Y)
    End Sub

End Class